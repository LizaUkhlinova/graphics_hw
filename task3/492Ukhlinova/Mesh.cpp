#include "Mesh.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

glm::vec3 getBreatherSurfaceVertice(float b, float u, float v) {
    float r = 1 - b * b;
    float w = sqrt(r);
    float denom = b * ((w * cosh(b * u))*(w * cosh(b * u)) + (b * sin(w * v))*(b * sin(w * v)));

    float x = -u + (2 * r * cosh(b * u) * sinh(b * u) / denom);
    float y = 2 * w * cosh(b * u) * (-(w * cos(v) * cos(w * v)) - (sin(v) * sin(w * v))) / denom;
    float z = 2 * w * cosh(b * u) * (-(w * sin(v) * cos(w * v)) + (cos(v) * sin(w * v))) / denom;

    return glm::vec3(x, y, z);
}

glm::vec3 getNormal(float b, float u, float v) {
    float r = 1 - b * b;
    float w = sqrt(r);
    float denom = b * ((w * cosh(b * u))*(w * cosh(b * u)) + (b * sin(w * v))*(b * sin(w * v)));

    float nx = 4 * b * w * w / (denom * denom) * sinh(b * u) * cosh(b * u) * ((w * w * sin(v) * sin(w * v) - sin(v) * sin(v * w)) * (- w * cos(v) * cos(v * w) - sin(v) * sin(w * v)) - (w * w - 1) * cos(v) * sin(v * w) * (- w * cos(v * w) * sin(v) + cos(v) * sin(v * w)));
    float ny = - 2 * w * cosh(b * u) / denom * (w * w - 1) * sin(v) * sin(v * w) * (2 * b * r / denom * (sinh(b * u) * sinh(b * u) + cosh(b * u) * cosh(b * u)) - 1);
    float nz = 2 * w * cosh(b * u) / denom * (w * w - 1) * cos(v) * sin(v * w) * (2 * b * r / denom * (sinh(b * u) * sinh(b * u) + cosh(b * u) * cosh(b * u)) - 1);
    float n0 = sqrt(nx * nx + ny * ny + nz * nz);
    return glm::vec3((-1.0f) * nx / n0, ny / n0, nz / n0);
}

MeshPtr makeBreatherSurface(float b, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords1;
    std::vector<glm::vec2> texcoords2;

    float u_min = -14;
    float u_max = 14;

    float v_min = -37.4;
    float v_max = 38;

    for (unsigned int i = 0; i < M; i++)
    {
        float u_1 = u_min + (u_max - u_min) * i / M;
        float u_2 = u_min + (u_max - u_min) * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float v_1 = v_min + (v_max - v_min) * j / N;
            float v_2 = v_min + (v_max - v_min) * (j + 1) / N;

            //1
            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_1));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_2));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_1));

            normals.push_back(getNormal(b, u_1, v_1));
            normals.push_back(getNormal(b, u_2, v_2));
            normals.push_back(getNormal(b, u_2, v_1));

            texcoords1.push_back(glm::vec2((float)j / N, (float)i / M));
            texcoords1.push_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));
            texcoords1.push_back(glm::vec2((float)j / N, (float)(i + 1) / M));

            texcoords2.push_back(glm::vec2((float)j / N, (float)i / M));
            texcoords2.push_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));
            texcoords2.push_back(glm::vec2((float)j / N, (float)(i + 1) / M));

            //2
            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_1));
            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_2));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_2));

            normals.push_back(getNormal(b, u_1, v_1));
            normals.push_back(getNormal(b, u_1, v_2));
            normals.push_back(getNormal(b, u_2, v_2));

            texcoords1.push_back(glm::vec2((float)j / N, (float)i / M));
            texcoords1.push_back(glm::vec2((float)(j + 1) / N, (float)i / M));
            texcoords1.push_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));

            texcoords2.push_back(glm::vec2((float)j / N, (float)i / M));
            texcoords2.push_back(glm::vec2((float)(j + 1) / N, (float)i / M));
            texcoords2.push_back(glm::vec2((float)(j + 1) / N, (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords1.size() * sizeof(float) * 2, texcoords1.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(texcoords2.size() * sizeof(float) * 2, texcoords2.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 2, GL_FLOAT, GL_FALSE, 0, 0, buf3);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Breathe Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));

        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeMirror(float size) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    // first triangle
    vertices.push_back(glm::vec3(0, -size, -size));
    vertices.push_back(glm::vec3(0, -size, size));
    vertices.push_back(glm::vec3(0, size, size));

    normals.push_back(glm::vec3(1, 0, 0));
    normals.push_back(glm::vec3(1, 0, 0));
    normals.push_back(glm::vec3(1, 0, 0));

    texcoords.push_back(glm::vec2(0, 0));
    texcoords.push_back(glm::vec2(0, 1));
    texcoords.push_back(glm::vec2(1, 1));

    // second triangle
    vertices.push_back(glm::vec3(0, size, size));
    vertices.push_back(glm::vec3(0, size, -size));
    vertices.push_back(glm::vec3(0, -size, -size));

    normals.push_back(glm::vec3(1, 0, 0));
    normals.push_back(glm::vec3(1, 0, 0));
    normals.push_back(glm::vec3(1, 0, 0));

    texcoords.push_back(glm::vec2(1, 1));
    texcoords.push_back(glm::vec2(1, 0));
    texcoords.push_back(glm::vec2(0, 0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Mirror is created with " << vertices.size() << " vertices\n";

  return mesh;
}
