#include "Application.h"
#include "Mesh.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include "LightInfo.h"

#include <vector>
#include <sstream>
#include <iostream>

class SampleApplication : public Application 
{
public:
	MeshPtr _breatherSurface;
	MeshPtr _marker; //Маркер для источника света
	MeshPtr _mirror; // зеркало

	//Идентификатор шейдерной программы
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    ShaderProgramPtr _mirrorShader;

    //Переменные для управления положением одного источника света
    float _lr = 4.5f;//10.0f;
    float _phi = 0.5f;//-5.0f;
    float _theta = glm::pi<float>() * 0.2f;

    LightInfo _light;

    TexturePtr _marbleTexture1;
    TexturePtr _marbleTexture2;
    TexturePtr _mirrorTex;

    GLuint _sampler1;
    GLuint _sampler2;

    GLuint _framebuffer;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;

	unsigned N = 500; //количество полигонов
	double b = 0.4; //параметр фигуры

	//Управление анимацией
    float _animationSpeed = 0.2;
    float _textureMerge = 1.0;
    float _shift = 0.0;
    bool stopAnimation = false;

    //Переменные для управления положением зеркала
    float _mirrorSize = 5.0f;

	float _mirrorX = 5.0f;
    float _mirrorPhi = 0.0f;
    float _mirrorTheta = glm::pi<float>() * 1.0f;
   	float _mirrorAlpha = 0.0f;
    float _mirrorBeta = 0.0f;
    float _mirrorGamma = 0.0f;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш с Breather Surface
		_breatherSurface = makeBreatherSurface(b, N);
		//_breatherSurface = makeSphere(5.0f);
		_breatherSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		_marker = makeSphere(1.0f);

		_mirror = makeMirror(_mirrorSize);

		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("492UkhlinovaData/texture_shader.vert", "492UkhlinovaData/texture_shader.frag");
		_markerShader = std::make_shared<ShaderProgram>("492UkhlinovaData/marker.vert", "492UkhlinovaData/marker.frag");
		_mirrorShader = std::make_shared<ShaderProgram>("492UkhlinovaData/mirror.vert", "492UkhlinovaData/mirror.frag");
		
		//=========================================================
        //Инициализация значений переменных освещения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8); 
        _light.specular = glm::vec3(1.0, 1.0, 1.0); 

		//=========================================================
        //Загрузка и создание текстур
		_marbleTexture1 = loadTexture("492UkhlinovaData/marble.jpg");
		_marbleTexture2 = loadTexture("492UkhlinovaData/marble2.jpg");

		//=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
	    glGenSamplers(1, &_sampler1);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	    glGenSamplers(1, &_sampler2);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

		//Создаем фреймбуфер
        glGenFramebuffers(1, &_framebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);

        //Создаем буфер глубины для фреймбуфера
        GLuint depthRenderBuffer;
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

	    _mirrorTex = std::make_shared <Texture> (GL_TEXTURE_2D);
	    _mirrorTex->setTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _fbWidth, _fbHeight, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	    _mirrorTex->attachToFramebuffer(GL_COLOR_ATTACHMENT0);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);

		if (ImGui::Begin("Breather Surface", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
			if (ImGui::CollapsingHeader("Animation"))
            {
				ImGui::Checkbox("Stop Animation", &stopAnimation);
			    ImGui::SliderFloat("Texture", &_textureMerge, 0.0f, 1.0f);
			    ImGui::SliderFloat("Animation speed", &_animationSpeed, 0.0f, 0.5f);
			}

			if (ImGui::CollapsingHeader("Light"))
            {
	            ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
	            ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
	            ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

	            ImGui::SliderFloat("radius", &_lr, 5.0f, 20.0f);
	            ImGui::SliderFloat("phi", &_phi, -5.0f, 2.0f * glm::pi<float>());
            	ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

            if (ImGui::CollapsingHeader("Mirror"))
            {
		        ImGui::SliderFloat("mirror center", &_mirrorX, 0.0f, 30.0f);
		        ImGui::SliderFloat("mirror phi", &_mirrorPhi, -3.142f, glm::pi<float>());
		        ImGui::SliderFloat("mirror theta", &_mirrorTheta, 0.0f, 2.0f * glm::pi<float>());
		        ImGui::SliderFloat("mirror alpha", &_mirrorAlpha, -glm::pi<float>(), glm::pi<float>());
		        ImGui::SliderFloat("mirror beta", &_mirrorBeta, -glm::pi<float>(), glm::pi<float>());
		        ImGui::SliderFloat("mirror gamma", &_mirrorGamma, -glm::pi<float>(), glm::pi<float>());
	    	}

		}
		ImGui::End();

	}

	void handleKey(int key, int scancode, int action, int mods) override
    {

        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
        	if (key == GLFW_KEY_SPACE) {
        		stopAnimation = !stopAnimation;
        	}	
        }
	}

	glm::mat4 mirrorModelMatrix() {
	  	glm::mat4 alphaRotateMatrix = glm::rotate(glm::mat4(1.0f), _mirrorAlpha, glm::vec3(0, 0, 1));
		glm::mat4 betaRotateMatrix =  glm::rotate(glm::mat4(1.0f), _mirrorBeta, glm::vec3(0, 1, 0));
		glm::mat4 gammaRotateMatrix = glm::rotate(glm::mat4(1.0f), _mirrorGamma, glm::vec3(1, 0, 0));
		glm::vec3 pos = glm::vec3(glm::cos(_mirrorPhi) * glm::cos(_mirrorTheta), glm::sin(_mirrorPhi) * glm::cos(_mirrorTheta), glm::sin(_mirrorTheta)) * _mirrorX;
		return glm::translate(glm::mat4(1.0f), pos) * gammaRotateMatrix * betaRotateMatrix * alphaRotateMatrix;
	}

	glm::mat4 mirrorViewMatrix() {
		glm::mat4 modelMatrix = mirrorModelMatrix();
	    glm::vec3 A = glm::inverse(_camera.viewMatrix) * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f); //вектор точки поверхности, которую мы хотим отразить
	    glm::vec3 B = modelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f); //вектор точки на зеркале
	    glm::vec3 D = A - B; //вектор от точки зеркала к точке поверхности
	    //для отражения A отразим вектор D и прибавим к нему B

	    glm::vec3 mirrorNormal = modelMatrix * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f);
	    glm::vec3 D_parallNormal = mirrorNormal * glm::dot(D, mirrorNormal);
	    glm::vec3 D_reflected = D - 2.0f * D_parallNormal;
	    glm::vec3 A_reflected = B + D_reflected;

	    glm::vec3 mirrorCoord = (A + A_reflected) / 2.0f;
	    glm::vec3 upVec = modelMatrix * glm::vec4(0.0f, 0.0f, -1.0f, 0.0f);
	    return glm::lookAt(A_reflected, mirrorCoord, upVec);
	}

	glm::mat4 mirrorProjMatrix() {
		glm::mat4 modelMatrix = mirrorModelMatrix();
		glm::mat4 viewMatrix = mirrorViewMatrix();
		float left = glm::vec3(viewMatrix * modelMatrix * glm::vec4(0.0f, -_mirrorSize , 0.0f, 1.0f)).x;
		float right = glm::vec3(viewMatrix * modelMatrix * glm::vec4(0.0f, _mirrorSize , 0.0f, 1.0f)).x;
		float bottom = glm::vec3(viewMatrix * modelMatrix * glm::vec4(0.0f, 0.0f, -_mirrorSize, 1.0f)).y;
		float top = glm::vec3(viewMatrix * modelMatrix * glm::vec4(0.0f, 0.0f, _mirrorSize, 1.0f)).y;
		float near = glm::abs(glm::vec3(viewMatrix * modelMatrix * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f)).z);
		float far = 100.0f;
		return glm::frustum(left, right, bottom, top, near, far);
	}

	void draw() override
	{
		Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Режим заливки полигона
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //Подключаем шейдер	
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
	    glBindSampler(0, _sampler1);
	    _marbleTexture1->bind();
	    _shader->setIntUniform("diffuseTex1", 0);

	    glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
	    glBindSampler(1, _sampler2);
	    _marbleTexture2->bind();
	    _shader->setIntUniform("diffuseTex2", 1);
        
  		glm::mat4 _mirrorModelMatrix = mirrorModelMatrix();
	    glm::mat4 _mirrorViewMatrix = mirrorViewMatrix();
	    glm::mat4 _mirrorProjMatrix = mirrorProjMatrix();
	    
  		if (!stopAnimation) {
			_shift = glfwGetTime() * _animationSpeed;
	  		_textureMerge = abs(cos(_shift));
  		}
        
        _shader->setFloatUniform("shift", _shift);
        _shader->setFloatUniform("textureMerge", _textureMerge);

	    {
	      _shader->setMat4Uniform("modelMatrix", _breatherSurface->modelMatrix());

	      //отраженная фигура в текстуре зеркала
	      glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
	      glViewport(0, 0, _fbWidth, _fbHeight);
	      glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	      _shader->setVec3Uniform("light.pos", glm::vec3(_mirrorViewMatrix * glm::vec4(_light.position, 1.0f)));
	      _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_mirrorViewMatrix * _breatherSurface->modelMatrix()))));
	      _shader->setMat4Uniform("viewMatrix", _mirrorViewMatrix);
	      _shader->setMat4Uniform("projectionMatrix", _mirrorProjMatrix);
	      
	      _breatherSurface->draw();

	      //настоящая фигура
	      glBindFramebuffer(GL_FRAMEBUFFER, 0);
	      glViewport(0, 0, width, height);
	      glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	      _shader->setVec3Uniform("light.pos", glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0f)));
	      _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _breatherSurface->modelMatrix()))));
	      _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
	      _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
	      
	      _breatherSurface->draw();
	    }

		{
	      _mirrorShader->use();

	      glActiveTexture(GL_TEXTURE0);
	      glBindSampler(0, _sampler1);
	      _mirrorTex->bind();

	      _mirrorShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * _mirrorModelMatrix);
	      _mirrorShader->setMat4Uniform("modelMatrix", _mirrorModelMatrix);
	      _mirrorShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

	      _mirrorShader->setIntUniform("mirrorTex", 0);

	      _mirrorShader->setVec3Uniform("light.pos", glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0f)));
	      _mirrorShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mirror->modelMatrix()))));
	      
	      _mirror->draw();
	    }

		{
			_markerShader->use();

			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
	        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
	        _marker->draw();
	    }

		glBindSampler(0, 0);
        glUseProgram(0);
	}
};

int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

    return 0;
}
